import { Component, Input } from '@angular/core';
import { IConfigMap } from '@mugan86/ng-leaflet';
import { DataService } from './data.service';
import { Map, geoJSON, circleMarker } from 'leaflet';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Input() config?: IConfigMap = {
    fullscreen: true
  };
  map!: Map;

  constructor(private dataService: DataService) { }
  getColor(numberValue: number) {
    return numberValue >= 0 && numberValue < 1
      ? "white"
      : numberValue >= 1 && numberValue < 2
        ? "green"
        : numberValue >= 2 && numberValue < 3
          ? "#6e8c51"
          : numberValue >= 3 && numberValue < 4
            ? "yellow"
            : numberValue >= 4 && numberValue < 5
              ? "#f5d142"
              : numberValue >= 5 && numberValue < 6
                ? "orange"
                : numberValue >= 6 && numberValue < 7
                  ? "red"
                  : "pink";
  }

  style = (feature: any) => {
    return {
      fillColor: this.getColor(feature.properties.mag),
      weight: 1,
      opacity: 1,
      color: "white",
      dashArray: "1",
      fillOpacity: 1,
    };
  }

  setMap($event: Map) {
    // Take current map with add configurations to use to expand with doc info
    this.map = $event;

    this.dataService.getEarthQuakesToday().subscribe((data) => {

      const geoJsonValue = geoJSON(data as any, {
        onEachFeature: (feature: any, layer: any) => {
          layer.bindPopup(
            "<h1>" +
            feature.properties.mag +
            "</h1><p>name: " +
            feature.properties.place +
            "</p>"
          );
        },
        // https://leafletjs.com/reference-1.7.1.html#geojson-pointtolayer
        // Esto se usa porque en el GeoJSON tenemos las features "Point"
        pointToLayer: (feature, latlng) => {
          return circleMarker(latlng, { radius: 4.5 * feature.properties.mag });
        },
        style: this.style,
      }).addTo(this.map);

      this.map.fitBounds([
        [
          geoJsonValue.getBounds().getNorthEast().lat,
          geoJsonValue.getBounds().getNorthEast().lng,
        ],
        [
          geoJsonValue.getBounds().getSouthWest().lat,
          geoJsonValue.getBounds().getSouthWest().lng,
        ],
      ]);
    })
  }
}
