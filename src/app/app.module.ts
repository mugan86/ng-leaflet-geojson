import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IConfigMap, NgLeafletModule, tileLayers } from '@mugan86/ng-leaflet';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

const config: IConfigMap = {
  fullscreen: true,
  center: [42,2],
  watermark: {
    show: true
  },
  layers: {
    baseLayers: [{
      label: 'Carto - Positron',
      map: tileLayers.baseLayers.cartoDb.map.positron,
      atribution: tileLayers.baseLayers.cartoDb.atribution,
      default: true
    },
    {
      label: 'Cyclo OSM',
      map: tileLayers.baseLayers.cycloOsm.map,
      atribution: tileLayers.baseLayers.cycloOsm.atribution
    }]
  }
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgLeafletModule.forRoot(config, {
      width: '100%',
      height: '600px'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
